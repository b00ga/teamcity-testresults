#!/bin/sh -eux

if ! rpm -q firefox; then
   yum install -y firefox glib2
fi
if ! rpm -q xauth; then
   yum install -y xauth
fi
if ! rpm -q glibc.i686; then
   yum install -y glibc.i686 glibc.x86_64
fi
if ! rpm -q libgcc.i686; then
   yum install -y libgcc.i686 libgcc.x86_64
fi

cd /vagrant

# https://www.digitalocean.com/community/tutorials/how-to-install-java-on-centos-and-fedora
if [ ! -f jdk-8u74-linux-i586.rpm ]; then
   wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u74-b02/jdk-8u74-linux-i586.rpm
fi
if ! rpm -q jdk1.8.0_74; then
   yum install -y jdk-8u74-linux-i586.rpm
fi

if [ ! -f TeamCity-9.1.6.tar.gz ]; then
   wget https://download.jetbrains.com/teamcity/TeamCity-9.1.6.tar.gz
fi
mkdir -p /opt/jetbrains
if [ ! -d /opt/jetbrains/TeamCity ]; then
   tar xvz -C /opt/jetbrains -f TeamCity-9.1.6.tar.gz
fi

#echo "teamcity.data.path=/opt/jetbrains/BuildServer" > /opt/jetbrains/TeamCity/conf/teamcity-startup.properties
#cp -fpr /vagrant/tc/05-antjunitjob/BuildServer /opt/jetbrains
/opt/jetbrains/TeamCity/bin/teamcity-server.sh start
/opt/jetbrains/TeamCity/buildAgent/bin/agent.sh start


yum install -y ant apache-ivy junit
ln -s /usr/share/java/ivy.jar /opt/jetbrains/TeamCity/buildAgent/plugins/ant/lib/ivy.jar
ln -s /usr/share/java/junit4.jar /opt/jetbrains/TeamCity/buildAgent/plugins/ant/lib/junit4.jar
alternatives --set java  /usr/java/jdk1.8.0_74/jre/bin/java
alternatives --set javac /usr/java/jdk1.8.0_74/bin/javac

